import React, { Component } from 'react';

class UpdateApp extends Component {

  constructor(props) {
    super(props);

    this.setCompany = this.setCompany.bind(this);
    this.setDuration = this.setDuration.bind(this);
    this.getInformation = this.getInformation.bind(this);

    this.state = {
      duration: "DAILY",
      companyArr: [],
      apiurl: null,
      response: [],
      highValues: [],
      lowValues: [],
      maxValue: null,
      minValue: null,
      weekDays: [
        "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"
      ],
      responseState: false,
      days: {
        Monday: [],
        Tuesday: [],
        Wednesday: [],
        Thursday: [],
        Friday: []
      },
      avg: 0,
      total: 0,
      weekAverage: []
    };
  }

  setCompany(event) {
    this.setState({
      company: event.target.value
    });
  }

  setDuration(event) {
    this.setState({
      duration: event.target.value
    });
  }

  callAPI() {
    var comp = this.state.company;
    var res = comp.split(",");
    this.state.companyArr = res;
    
    for (var i = 0; i < 6; i++) {
      var api = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=' + res[i] + '&interval=1min&apikey=H2Q9LT2BZBLNV9KN';
      fetch(api).then(data => data.json()).then(
        resText => {
          this.setState({
            response: resText['Time Series (Daily)']
          });

          this.processDataMinMax();
          this.processDataWeekly();
        }
      ).catch(function (error) {
        console.log(error);
      });
    }
  }

  processDataWeekly() {
    let dataToProcess = this.state.response;

    for (var key in dataToProcess) {
      if (typeof dataToProcess[key] !== 'undefined') {
        var date = new Date(key);
        if (date.getDay() === 1) {
          this.state.days.Monday.push(dataToProcess[key]['2. high']);
        }
        else if (date.getDay() === 2) {
          this.state.days.Tuesday.push(dataToProcess[key]['2. high']);
        }
        else if (date.getDay() === 3) {
          this.state.days.Wednesday.push(dataToProcess[key]['2. high']);
        }
        else if (date.getDay() === 4) {
          this.state.days.Thursday.push(dataToProcess[key]['2. high']);
        }
        else if (date.getDay() === 5) {
          this.state.days.Friday.push(dataToProcess[key]['2. high']);
        }
      }
      else {
        console.log('Value not found');
      }
    }
    for (var dayNum = 0; dayNum < 5; dayNum++) {
      var dayName = this.state.weekDays[dayNum];
      var dayArrayLength = this.state.days[dayName].length;
      this.calculateAverage(dayName, dayArrayLength);
    }

    this.setState({
      responseState: true
    })
  }

  calculateAverage(name, elementsLength) {
    var sum = 0;
    for (var i = 0; i < elementsLength; i++) {
      sum += parseInt(this.state.days[name][i], 10);
    }
    var avg = sum / elementsLength;
    this.state.weekAverage.push(avg);
  }

  processDataMinMax() {
    let dataToProcess = this.state.response;
    for (var key in dataToProcess) {
      if (typeof dataToProcess[key] !== 'undefined') {
        this.state.highValues.push(dataToProcess[key]['2. high']);
        this.state.lowValues.push(dataToProcess[key]['3. low']);
      }
      else {
        console.log('Value not found');
      }
    }
    this.setMinMaxValues();
  }

  setMinMaxValues() {
    this.setState({
      maxValue: Math.max(...this.state.highValues),
      minValue: Math.max(...this.state.lowValues)
    });
  }

  getInformation(event) {
    this.callAPI();
  }

  render() {
  
    return (
      <div>
      <input type="text" value={this.state.company} onChange={this.setCompany} placeholder="Enter Company Name" />
      <br />
      <br />
      <select onChange={this.setDuration} value={this.state.duration}>
        <option value="DAILY">Daily</option>
        <option value="WEEKLY">Weekly</option>
        <option value="MONTHLY">Monthly</option>
      </select>

      <button type="button" className="btn btn-primary" onClick={this.getInformation}>Get Information</button>

      <h3>You have Searched for :  {this.state.company}</h3>

      <PrintData maxValue={this.state.maxValue} minValue={this.state.minValue} weekDays={this.state.weekDays} weekAverage={this.state.weekAverage} responseState={this.state.responseState} ></PrintData>

    </div>);
  }
}


class PrintData extends Component {
  constructor(props) {
    super(props);
  }
  getTable() {
    if (this.props.responseState === true) {
      var table =
        <div>
          <table>
            <tbody>
              {this.props.weekDays.map((val, i) => {
                return (
                  <tr>
                    <td>{val}</td>
                    <td>{this.props.weekAverage[i]}</td>
                  </tr>
                )
              })
              }
            </tbody>
          </table>
        </div>;

      return (table);
    }
  }
  printMinMax() {
    if (this.props.responseState === true) {
      var minMaxData =
        <div>
          <h3> Min Value : {this.props.minValue}</h3>
          <h3> Max Value : {this.props.maxValue}</h3>
        </div>;
      return (minMaxData);
    }
  }
  render() {
   let list = this.props.companyArr;
    
    return (
      <div>
        {this.printMinMax()}
        {this.getTable()}
      </div>
    );
  }
}
export default UpdateApp;
