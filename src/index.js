import React from 'react';
import ReactDOM from 'react-dom';
import UpdateApp from './UpdateApp';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<UpdateApp />, document.getElementById('root'));
registerServiceWorker();
